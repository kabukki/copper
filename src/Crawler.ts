export class Crawler {

    // private mutationObserver: MutationObserver;

    constructor () {
        // this.mutationObserver = new MutationObserver(this.onMutation.bind(this));
    }

    /**
     * Callback on dom mutation
     * @param mutations 
     */
    // onMutation (mutations: MutationRecord) {
    //     console.log(mutations);
    // }
	
	find<T extends Element = Element> (selector: string): T | null {
		return document.querySelector<T>(selector);
	}
	
	findInput (selector: string): HTMLInputElement | null {
		return this.find<HTMLInputElement>(selector);
	}
	
	/**
	 * Find a link to the given path from the current page. Should be overriden to be more specific
	 * @param path 
	 */
	findLink (path: string): HTMLElement {
		return this.find(path);
	}

	fillInput (element: HTMLInputElement, text: string) {
		element.value = text;
	}

    /**
     * Click on an element
     * @param element 
     */
    async click (element: HTMLElement): Promise<void> {
        // this.mutationObserver.observe(document.body, {
        //     attributes: true,
        //     subtree: true
        // });
        element.click();

        // this.mutationObserver.disconnect();
    }

}