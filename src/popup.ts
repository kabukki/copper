/**
 * Inject a script into the current tab
 * @param file relative path to the file to execute
 */
function executeScript (file) {
	chrome.tabs.query({
		currentWindow: true,
		active: true,
	}, ([tab]) => {
		// Load these content scripts in the isolated world
		chrome.tabs.executeScript(tab.id, {
			file,
			// runAt: 'document_end'
		}, console.log);
	});
}

function main () {
	const map = new Map();

	map.set('button#supreme', 'js/supreme.js');

	for (const [key, file] of map.entries()) {
		const button = document.querySelector(key);
		if (button) {
			button.addEventListener('click', _ => {
				executeScript(file);
			});
		}
	}
    
    chrome.storage.sync.get('test', ({ test }) => {
        // button.innerHTML = test.toString();
    });
}

window.addEventListener('DOMContentLoaded', main);
