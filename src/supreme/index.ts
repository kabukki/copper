/**
 * This script is intended to be run in the context of the Supreme online shop.
 */

import { Supreme } from './Supreme';

let supreme = new Supreme();

function goToShop () {
	if (!window.location.href.includes(Supreme.URLS.SHOP.BASE)) {
		const shopLink = supreme.findLink(Supreme.URLS.SHOP.BASE);
		if (shopLink) {
			shopLink.click();
		} else {
			throw new Error('Cannot find a link to the shop. Aborting');
		}
	} else {
		console.warn('Already on shop page');
	}
}

try {
	// goToShop();
	supreme.checkout();
} catch (err) {
	console.error(err);
}

// setInterval(_ => {
//     console.log(window.location.href);
// }, 1000)
