export interface ICheckoutInformation {
	name: string;
	email: string;
	phone: string;
	address: string;
	appt: string;
	zipcode: string;
	city: string;
	state: string;
	country: string;
	number: string;
	expMonth: string;
	expYear: string;
	cvv: string;
};
