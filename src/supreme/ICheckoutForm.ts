export interface ICheckoutForm {
	name: HTMLInputElement;
	email: HTMLInputElement;
	phone: HTMLInputElement;
	address: HTMLInputElement;
	appt: HTMLInputElement;
	zipcode: HTMLInputElement;
	city: HTMLInputElement;
	state: HTMLInputElement;
	country: HTMLInputElement;
	number: HTMLInputElement;
	expMonth: HTMLInputElement;
	expYear: HTMLInputElement;
	cvv: HTMLInputElement;
	tos: HTMLInputElement;
	submit: HTMLInputElement;
};
