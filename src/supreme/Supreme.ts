import { ISeller } from '../ISeller';
import { Crawler } from '../Crawler';
import { ICheckoutInformation } from './ICheckoutInformation';
import { ICheckoutForm } from './ICheckoutForm';

// IDEA plugin system, add new classes. Define Interfaces (TS)

export class Supreme extends Crawler implements ISeller {

	findLink (path: string): HTMLElement {
		return this.find(path)
        || this.find(`http://${Supreme.hostname}${path}`)
        || this.find(`https://${Supreme.hostname}${path}`);
	}

    getProducts () {
        for (const category of Supreme.categories) {
            const link = this.findLink(`/shop/all/${category}`);
            if (!link) continue;
            this.click(link);
        }
        return [];
    }

	/**
	 * Get checkout information (data needed by the form)
	 */
	getCheckoutInformation (): ICheckoutInformation {
		return {
			name: 'John Doe',
			email: 'dummy@email.lol',
			phone: '0000000000',
			address: '330 allée des Ursulines',
			appt: '302',
			zipcode: 'G5L 2Z9',
			city: 'Rimouski',
			country: 'CANADA',
			state: 'QC',
			number: '5100000000000016',
			expMonth: '12',
			expYear: '2021',
			cvv: '123',
		};
		// Must set country first, then state
	}

	/**
	 * Get checkout form elements
	 */
	getCheckoutForm (): ICheckoutForm {
		let elements: ICheckoutForm = {} as ICheckoutForm;

		for (const [key, selector] of Object.entries(Supreme.checkoutSelectors)) {
			elements[key] = document.querySelector(selector);
		}
		return elements;
	}

	/**
	 * Fill checkout information and proceed.
	 * Needs to already be on checkout page.
	 */
	checkout () {
		let data: ICheckoutInformation = this.getCheckoutInformation();
		let elements: ICheckoutForm = this.getCheckoutForm();
		
		try {
			// Fill regular input
			for (const key in data) {
				this.fillInput(elements[key], data[key]);
			}
			// Check TOS checkbox
			elements.tos.checked = true;
			// Submit form
			this.click(elements.submit);
			// const e = this.findInput(Supreme.checkoutSelectors.name);
			// this.fillInput(e, 'lol');
		} catch (err) {
			console.log(err);
		}
	}

    /**
     * Website hostname
     */
    static get hostname () {
        return 'www.supremenewyork.com';
    }

    /**
     * Available product categories
     */
    static get categories (): string[] {
        return [
            'jackets',
            'shirts',
            'tops/sweaters',
            'sweatshirts',
            'pants',
            'hats',
            'bags',
            'accessories',
            'skate'
        ];
	}
	
	/**
	 * CSS selectors on the checkout page
	 * NOTE: only on US version only
	 */
	static get checkoutSelectors () {
		return {
			name: '#order_billing_name',
			email: '#order_email',
			phone: '#order_tel',
			address: '#bo',
			appt: '#oba3',
			zipcode: '#order_billing_zip',
			city: '#order_billing_city',
			country: '#order_billing_country',
			state: '#order_billing_state',
			number: '#nnaerb',
			expMonth: '#credit_card_month',
			expYear: '#credit_card_year',
			cvv: '#orcer',
			tos: '#order_terms',
			submit: '#pay > input'
		};
	}

    /**
     * Navigation urls
     */
    static get URLS () {
        return {
            BASE: '/',
            SHOP: {
                BASE: '/shop',
                ALL: '/shop/all',
                NEW: '/shop/new',
                JACKETS: '/shop/jackets',
                SHIRTS: '/shop/shirts',
                TOPS_SWEATERS: '/shop/tops_sweaters',
                SWEATSHIRTS: '/shop/sweatshirts',
                PANTS: '/shop/pants',
                HATS: '/shop/hats',
                BAGS: '/shop/bags',
                ACCESSORIES: '/shop/accessories',
                SKATES: '/shop/skate',
            },
            CART: '/cart',
            CHECKOUT: '/checkout',
        };
    }

}
