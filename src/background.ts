import { Supreme } from './supreme/Supreme';

/**
 * List of available sellers
 */
const allSellers = [Supreme];

function setupStorage () {
    chrome.storage.sync.set({
        test: true
    }, () => {
        console.log('okay storage is set');
    });
}

/**
 * Enable page action when on a seller's website
 */
function setupDeclarativeContent () {
    chrome.declarativeContent.onPageChanged.removeRules(undefined, () => {
        chrome.declarativeContent.onPageChanged.addRules(allSellers.map(seller =>
            ({
                conditions: [
                    new chrome.declarativeContent.PageStateMatcher({
                        pageUrl: {
                            hostEquals: seller.hostname
                        }
                    })
                ],
                actions: [
                    new chrome.declarativeContent.ShowPageAction()
                ]
            })
        ));
    });
}

chrome.runtime.onInstalled.addListener(_ => {
    setupStorage();
    setupDeclarativeContent();
});
