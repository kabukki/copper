/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/background.ts");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/Crawler.ts":
/*!************************!*\
  !*** ./src/Crawler.ts ***!
  \************************/
/*! exports provided: Crawler */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Crawler", function() { return Crawler; });
class Crawler {
    // private mutationObserver: MutationObserver;
    constructor() {
        // this.mutationObserver = new MutationObserver(this.onMutation.bind(this));
    }
    /**
     * Callback on dom mutation
     * @param mutations
     */
    // onMutation (mutations: MutationRecord) {
    //     console.log(mutations);
    // }
    find(selector) {
        return document.querySelector(selector);
    }
    findInput(selector) {
        return this.find(selector);
    }
    /**
     * Find a link to the given path from the current page. Should be overriden to be more specific
     * @param path
     */
    findLink(path) {
        return this.find(path);
    }
    fillInput(element, text) {
        element.value = text;
    }
    /**
     * Click on an element
     * @param element
     */
    async click(element) {
        // this.mutationObserver.observe(document.body, {
        //     attributes: true,
        //     subtree: true
        // });
        element.click();
        // this.mutationObserver.disconnect();
    }
}


/***/ }),

/***/ "./src/background.ts":
/*!***************************!*\
  !*** ./src/background.ts ***!
  \***************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _supreme_Supreme__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./supreme/Supreme */ "./src/supreme/Supreme.ts");

/**
 * List of available sellers
 */
const allSellers = [_supreme_Supreme__WEBPACK_IMPORTED_MODULE_0__["Supreme"]];
function setupStorage() {
    chrome.storage.sync.set({
        test: true
    }, () => {
        console.log('okay storage is set');
    });
}
/**
 * Enable page action when on a seller's website
 */
function setupDeclarativeContent() {
    chrome.declarativeContent.onPageChanged.removeRules(undefined, () => {
        chrome.declarativeContent.onPageChanged.addRules(allSellers.map(seller => ({
            conditions: [
                new chrome.declarativeContent.PageStateMatcher({
                    pageUrl: {
                        hostEquals: seller.hostname
                    }
                })
            ],
            actions: [
                new chrome.declarativeContent.ShowPageAction()
            ]
        })));
    });
}
chrome.runtime.onInstalled.addListener(_ => {
    setupStorage();
    setupDeclarativeContent();
});


/***/ }),

/***/ "./src/supreme/Supreme.ts":
/*!********************************!*\
  !*** ./src/supreme/Supreme.ts ***!
  \********************************/
/*! exports provided: Supreme */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Supreme", function() { return Supreme; });
/* harmony import */ var _Crawler__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../Crawler */ "./src/Crawler.ts");

// IDEA plugin system, add new classes. Define Interfaces (TS)
class Supreme extends _Crawler__WEBPACK_IMPORTED_MODULE_0__["Crawler"] {
    findLink(path) {
        return this.find(path)
            || this.find(`http://${Supreme.hostname}${path}`)
            || this.find(`https://${Supreme.hostname}${path}`);
    }
    getProducts() {
        for (const category of Supreme.categories) {
            const link = this.findLink(`/shop/all/${category}`);
            if (!link)
                continue;
            this.click(link);
        }
        return [];
    }
    /**
     * Get checkout information (data needed by the form)
     */
    getCheckoutInformation() {
        return {
            name: 'John Doe',
            email: 'dummy@email.lol',
            phone: '0000000000',
            address: '330 allée des Ursulines',
            appt: '302',
            zipcode: 'G5L 2Z9',
            city: 'Rimouski',
            country: 'CANADA',
            state: 'QC',
            number: '5100000000000016',
            expMonth: '12',
            expYear: '2021',
            cvv: '123',
        };
        // Must set country first, then state
    }
    /**
     * Get checkout form elements
     */
    getCheckoutForm() {
        let elements = {};
        for (const [key, selector] of Object.entries(Supreme.checkoutSelectors)) {
            elements[key] = document.querySelector(selector);
        }
        return elements;
    }
    /**
     * Fill checkout information and proceed.
     * Needs to already be on checkout page.
     */
    checkout() {
        let data = this.getCheckoutInformation();
        let elements = this.getCheckoutForm();
        try {
            // Fill regular input
            for (const key in data) {
                this.fillInput(elements[key], data[key]);
            }
            // Check TOS checkbox
            elements.tos.checked = true;
            // Submit form
            this.click(elements.submit);
            // const e = this.findInput(Supreme.checkoutSelectors.name);
            // this.fillInput(e, 'lol');
        }
        catch (err) {
            console.log(err);
        }
    }
    /**
     * Website hostname
     */
    static get hostname() {
        return 'www.supremenewyork.com';
    }
    /**
     * Available product categories
     */
    static get categories() {
        return [
            'jackets',
            'shirts',
            'tops/sweaters',
            'sweatshirts',
            'pants',
            'hats',
            'bags',
            'accessories',
            'skate'
        ];
    }
    /**
     * CSS selectors on the checkout page
     * NOTE: only on US version only
     */
    static get checkoutSelectors() {
        return {
            name: '#order_billing_name',
            email: '#order_email',
            phone: '#order_tel',
            address: '#bo',
            appt: '#oba3',
            zipcode: '#order_billing_zip',
            city: '#order_billing_city',
            country: '#order_billing_country',
            state: '#order_billing_state',
            number: '#nnaerb',
            expMonth: '#credit_card_month',
            expYear: '#credit_card_year',
            cvv: '#orcer',
            tos: '#order_terms',
            submit: '#pay > input'
        };
    }
    /**
     * Navigation urls
     */
    static get URLS() {
        return {
            BASE: '/',
            SHOP: {
                BASE: '/shop',
                ALL: '/shop/all',
                NEW: '/shop/new',
                JACKETS: '/shop/jackets',
                SHIRTS: '/shop/shirts',
                TOPS_SWEATERS: '/shop/tops_sweaters',
                SWEATSHIRTS: '/shop/sweatshirts',
                PANTS: '/shop/pants',
                HATS: '/shop/hats',
                BAGS: '/shop/bags',
                ACCESSORIES: '/shop/accessories',
                SKATES: '/shop/skate',
            },
            CART: '/cart',
            CHECKOUT: '/checkout',
        };
    }
}


/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vLy4vc3JjL0NyYXdsZXIudHMiLCJ3ZWJwYWNrOi8vLy4vc3JjL2JhY2tncm91bmQudHMiLCJ3ZWJwYWNrOi8vLy4vc3JjL3N1cHJlbWUvU3VwcmVtZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxrREFBMEMsZ0NBQWdDO0FBQzFFO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsZ0VBQXdELGtCQUFrQjtBQUMxRTtBQUNBLHlEQUFpRCxjQUFjO0FBQy9EOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpREFBeUMsaUNBQWlDO0FBQzFFLHdIQUFnSCxtQkFBbUIsRUFBRTtBQUNySTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLG1DQUEyQiwwQkFBMEIsRUFBRTtBQUN2RCx5Q0FBaUMsZUFBZTtBQUNoRDtBQUNBO0FBQ0E7O0FBRUE7QUFDQSw4REFBc0QsK0RBQStEOztBQUVySDtBQUNBOzs7QUFHQTtBQUNBOzs7Ozs7Ozs7Ozs7O0FDbEZBO0FBQUE7QUFBTyxNQUFNLE9BQU87SUFFaEIsOENBQThDO0lBRTlDO1FBQ0ksNEVBQTRFO0lBQ2hGLENBQUM7SUFFRDs7O09BR0c7SUFDSCwyQ0FBMkM7SUFDM0MsOEJBQThCO0lBQzlCLElBQUk7SUFFUCxJQUFJLENBQStCLFFBQWdCO1FBQ2xELE9BQU8sUUFBUSxDQUFDLGFBQWEsQ0FBSSxRQUFRLENBQUMsQ0FBQztJQUM1QyxDQUFDO0lBRUQsU0FBUyxDQUFFLFFBQWdCO1FBQzFCLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBbUIsUUFBUSxDQUFDLENBQUM7SUFDOUMsQ0FBQztJQUVEOzs7T0FHRztJQUNILFFBQVEsQ0FBRSxJQUFZO1FBQ3JCLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUN4QixDQUFDO0lBRUQsU0FBUyxDQUFFLE9BQXlCLEVBQUUsSUFBWTtRQUNqRCxPQUFPLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQztJQUN0QixDQUFDO0lBRUU7OztPQUdHO0lBQ0gsS0FBSyxDQUFDLEtBQUssQ0FBRSxPQUFvQjtRQUM3QixpREFBaUQ7UUFDakQsd0JBQXdCO1FBQ3hCLG9CQUFvQjtRQUNwQixNQUFNO1FBQ04sT0FBTyxDQUFDLEtBQUssRUFBRSxDQUFDO1FBRWhCLHNDQUFzQztJQUMxQyxDQUFDO0NBRUo7Ozs7Ozs7Ozs7Ozs7QUNsREQ7QUFBQTtBQUE0QztBQUU1Qzs7R0FFRztBQUNILE1BQU0sVUFBVSxHQUFHLENBQUMsd0RBQU8sQ0FBQyxDQUFDO0FBRTdCLFNBQVMsWUFBWTtJQUNqQixNQUFNLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUM7UUFDcEIsSUFBSSxFQUFFLElBQUk7S0FDYixFQUFFLEdBQUcsRUFBRTtRQUNKLE9BQU8sQ0FBQyxHQUFHLENBQUMscUJBQXFCLENBQUMsQ0FBQztJQUN2QyxDQUFDLENBQUMsQ0FBQztBQUNQLENBQUM7QUFFRDs7R0FFRztBQUNILFNBQVMsdUJBQXVCO0lBQzVCLE1BQU0sQ0FBQyxrQkFBa0IsQ0FBQyxhQUFhLENBQUMsV0FBVyxDQUFDLFNBQVMsRUFBRSxHQUFHLEVBQUU7UUFDaEUsTUFBTSxDQUFDLGtCQUFrQixDQUFDLGFBQWEsQ0FBQyxRQUFRLENBQUMsVUFBVSxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsRUFBRSxDQUNyRSxDQUFDO1lBQ0csVUFBVSxFQUFFO2dCQUNSLElBQUksTUFBTSxDQUFDLGtCQUFrQixDQUFDLGdCQUFnQixDQUFDO29CQUMzQyxPQUFPLEVBQUU7d0JBQ0wsVUFBVSxFQUFFLE1BQU0sQ0FBQyxRQUFRO3FCQUM5QjtpQkFDSixDQUFDO2FBQ0w7WUFDRCxPQUFPLEVBQUU7Z0JBQ0wsSUFBSSxNQUFNLENBQUMsa0JBQWtCLENBQUMsY0FBYyxFQUFFO2FBQ2pEO1NBQ0osQ0FBQyxDQUNMLENBQUMsQ0FBQztJQUNQLENBQUMsQ0FBQyxDQUFDO0FBQ1AsQ0FBQztBQUVELE1BQU0sQ0FBQyxPQUFPLENBQUMsV0FBVyxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsRUFBRTtJQUN2QyxZQUFZLEVBQUUsQ0FBQztJQUNmLHVCQUF1QixFQUFFLENBQUM7QUFDOUIsQ0FBQyxDQUFDLENBQUM7Ozs7Ozs7Ozs7Ozs7QUN2Q0g7QUFBQTtBQUFBO0FBQXFDO0FBSXJDLDhEQUE4RDtBQUV2RCxNQUFNLE9BQVEsU0FBUSxnREFBTztJQUVuQyxRQUFRLENBQUUsSUFBWTtRQUNyQixPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO2VBQ2IsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLE9BQU8sQ0FBQyxRQUFRLEdBQUcsSUFBSSxFQUFFLENBQUM7ZUFDOUMsSUFBSSxDQUFDLElBQUksQ0FBQyxXQUFXLE9BQU8sQ0FBQyxRQUFRLEdBQUcsSUFBSSxFQUFFLENBQUMsQ0FBQztJQUMxRCxDQUFDO0lBRUUsV0FBVztRQUNQLEtBQUssTUFBTSxRQUFRLElBQUksT0FBTyxDQUFDLFVBQVUsRUFBRTtZQUN2QyxNQUFNLElBQUksR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLGFBQWEsUUFBUSxFQUFFLENBQUMsQ0FBQztZQUNwRCxJQUFJLENBQUMsSUFBSTtnQkFBRSxTQUFTO1lBQ3BCLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUM7U0FDcEI7UUFDRCxPQUFPLEVBQUUsQ0FBQztJQUNkLENBQUM7SUFFSjs7T0FFRztJQUNILHNCQUFzQjtRQUNyQixPQUFPO1lBQ04sSUFBSSxFQUFFLFVBQVU7WUFDaEIsS0FBSyxFQUFFLGlCQUFpQjtZQUN4QixLQUFLLEVBQUUsWUFBWTtZQUNuQixPQUFPLEVBQUUseUJBQXlCO1lBQ2xDLElBQUksRUFBRSxLQUFLO1lBQ1gsT0FBTyxFQUFFLFNBQVM7WUFDbEIsSUFBSSxFQUFFLFVBQVU7WUFDaEIsT0FBTyxFQUFFLFFBQVE7WUFDakIsS0FBSyxFQUFFLElBQUk7WUFDWCxNQUFNLEVBQUUsa0JBQWtCO1lBQzFCLFFBQVEsRUFBRSxJQUFJO1lBQ2QsT0FBTyxFQUFFLE1BQU07WUFDZixHQUFHLEVBQUUsS0FBSztTQUNWLENBQUM7UUFDRixxQ0FBcUM7SUFDdEMsQ0FBQztJQUVEOztPQUVHO0lBQ0gsZUFBZTtRQUNkLElBQUksUUFBUSxHQUFrQixFQUFtQixDQUFDO1FBRWxELEtBQUssTUFBTSxDQUFDLEdBQUcsRUFBRSxRQUFRLENBQUMsSUFBSSxNQUFNLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxpQkFBaUIsQ0FBQyxFQUFFO1lBQ3hFLFFBQVEsQ0FBQyxHQUFHLENBQUMsR0FBRyxRQUFRLENBQUMsYUFBYSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1NBQ2pEO1FBQ0QsT0FBTyxRQUFRLENBQUM7SUFDakIsQ0FBQztJQUVEOzs7T0FHRztJQUNILFFBQVE7UUFDUCxJQUFJLElBQUksR0FBeUIsSUFBSSxDQUFDLHNCQUFzQixFQUFFLENBQUM7UUFDL0QsSUFBSSxRQUFRLEdBQWtCLElBQUksQ0FBQyxlQUFlLEVBQUUsQ0FBQztRQUVyRCxJQUFJO1lBQ0gscUJBQXFCO1lBQ3JCLEtBQUssTUFBTSxHQUFHLElBQUksSUFBSSxFQUFFO2dCQUN2QixJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsRUFBRSxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQzthQUN6QztZQUNELHFCQUFxQjtZQUNyQixRQUFRLENBQUMsR0FBRyxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUM7WUFDNUIsY0FBYztZQUNkLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1lBQzVCLDREQUE0RDtZQUM1RCw0QkFBNEI7U0FDNUI7UUFBQyxPQUFPLEdBQUcsRUFBRTtZQUNiLE9BQU8sQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUM7U0FDakI7SUFDRixDQUFDO0lBRUU7O09BRUc7SUFDSCxNQUFNLEtBQUssUUFBUTtRQUNmLE9BQU8sd0JBQXdCLENBQUM7SUFDcEMsQ0FBQztJQUVEOztPQUVHO0lBQ0gsTUFBTSxLQUFLLFVBQVU7UUFDakIsT0FBTztZQUNILFNBQVM7WUFDVCxRQUFRO1lBQ1IsZUFBZTtZQUNmLGFBQWE7WUFDYixPQUFPO1lBQ1AsTUFBTTtZQUNOLE1BQU07WUFDTixhQUFhO1lBQ2IsT0FBTztTQUNWLENBQUM7SUFDVCxDQUFDO0lBRUQ7OztPQUdHO0lBQ0gsTUFBTSxLQUFLLGlCQUFpQjtRQUMzQixPQUFPO1lBQ04sSUFBSSxFQUFFLHFCQUFxQjtZQUMzQixLQUFLLEVBQUUsY0FBYztZQUNyQixLQUFLLEVBQUUsWUFBWTtZQUNuQixPQUFPLEVBQUUsS0FBSztZQUNkLElBQUksRUFBRSxPQUFPO1lBQ2IsT0FBTyxFQUFFLG9CQUFvQjtZQUM3QixJQUFJLEVBQUUscUJBQXFCO1lBQzNCLE9BQU8sRUFBRSx3QkFBd0I7WUFDakMsS0FBSyxFQUFFLHNCQUFzQjtZQUM3QixNQUFNLEVBQUUsU0FBUztZQUNqQixRQUFRLEVBQUUsb0JBQW9CO1lBQzlCLE9BQU8sRUFBRSxtQkFBbUI7WUFDNUIsR0FBRyxFQUFFLFFBQVE7WUFDYixHQUFHLEVBQUUsY0FBYztZQUNuQixNQUFNLEVBQUUsY0FBYztTQUN0QixDQUFDO0lBQ0gsQ0FBQztJQUVFOztPQUVHO0lBQ0gsTUFBTSxLQUFLLElBQUk7UUFDWCxPQUFPO1lBQ0gsSUFBSSxFQUFFLEdBQUc7WUFDVCxJQUFJLEVBQUU7Z0JBQ0YsSUFBSSxFQUFFLE9BQU87Z0JBQ2IsR0FBRyxFQUFFLFdBQVc7Z0JBQ2hCLEdBQUcsRUFBRSxXQUFXO2dCQUNoQixPQUFPLEVBQUUsZUFBZTtnQkFDeEIsTUFBTSxFQUFFLGNBQWM7Z0JBQ3RCLGFBQWEsRUFBRSxxQkFBcUI7Z0JBQ3BDLFdBQVcsRUFBRSxtQkFBbUI7Z0JBQ2hDLEtBQUssRUFBRSxhQUFhO2dCQUNwQixJQUFJLEVBQUUsWUFBWTtnQkFDbEIsSUFBSSxFQUFFLFlBQVk7Z0JBQ2xCLFdBQVcsRUFBRSxtQkFBbUI7Z0JBQ2hDLE1BQU0sRUFBRSxhQUFhO2FBQ3hCO1lBQ0QsSUFBSSxFQUFFLE9BQU87WUFDYixRQUFRLEVBQUUsV0FBVztTQUN4QixDQUFDO0lBQ04sQ0FBQztDQUVKIiwiZmlsZSI6ImJhY2tncm91bmQuanMiLCJzb3VyY2VzQ29udGVudCI6WyIgXHQvLyBUaGUgbW9kdWxlIGNhY2hlXG4gXHR2YXIgaW5zdGFsbGVkTW9kdWxlcyA9IHt9O1xuXG4gXHQvLyBUaGUgcmVxdWlyZSBmdW5jdGlvblxuIFx0ZnVuY3Rpb24gX193ZWJwYWNrX3JlcXVpcmVfXyhtb2R1bGVJZCkge1xuXG4gXHRcdC8vIENoZWNrIGlmIG1vZHVsZSBpcyBpbiBjYWNoZVxuIFx0XHRpZihpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSkge1xuIFx0XHRcdHJldHVybiBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXS5leHBvcnRzO1xuIFx0XHR9XG4gXHRcdC8vIENyZWF0ZSBhIG5ldyBtb2R1bGUgKGFuZCBwdXQgaXQgaW50byB0aGUgY2FjaGUpXG4gXHRcdHZhciBtb2R1bGUgPSBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSA9IHtcbiBcdFx0XHRpOiBtb2R1bGVJZCxcbiBcdFx0XHRsOiBmYWxzZSxcbiBcdFx0XHRleHBvcnRzOiB7fVxuIFx0XHR9O1xuXG4gXHRcdC8vIEV4ZWN1dGUgdGhlIG1vZHVsZSBmdW5jdGlvblxuIFx0XHRtb2R1bGVzW21vZHVsZUlkXS5jYWxsKG1vZHVsZS5leHBvcnRzLCBtb2R1bGUsIG1vZHVsZS5leHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKTtcblxuIFx0XHQvLyBGbGFnIHRoZSBtb2R1bGUgYXMgbG9hZGVkXG4gXHRcdG1vZHVsZS5sID0gdHJ1ZTtcblxuIFx0XHQvLyBSZXR1cm4gdGhlIGV4cG9ydHMgb2YgdGhlIG1vZHVsZVxuIFx0XHRyZXR1cm4gbW9kdWxlLmV4cG9ydHM7XG4gXHR9XG5cblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGVzIG9iamVjdCAoX193ZWJwYWNrX21vZHVsZXNfXylcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubSA9IG1vZHVsZXM7XG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlIGNhY2hlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmMgPSBpbnN0YWxsZWRNb2R1bGVzO1xuXG4gXHQvLyBkZWZpbmUgZ2V0dGVyIGZ1bmN0aW9uIGZvciBoYXJtb255IGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uZCA9IGZ1bmN0aW9uKGV4cG9ydHMsIG5hbWUsIGdldHRlcikge1xuIFx0XHRpZighX193ZWJwYWNrX3JlcXVpcmVfXy5vKGV4cG9ydHMsIG5hbWUpKSB7XG4gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIG5hbWUsIHsgZW51bWVyYWJsZTogdHJ1ZSwgZ2V0OiBnZXR0ZXIgfSk7XG4gXHRcdH1cbiBcdH07XG5cbiBcdC8vIGRlZmluZSBfX2VzTW9kdWxlIG9uIGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uciA9IGZ1bmN0aW9uKGV4cG9ydHMpIHtcbiBcdFx0aWYodHlwZW9mIFN5bWJvbCAhPT0gJ3VuZGVmaW5lZCcgJiYgU3ltYm9sLnRvU3RyaW5nVGFnKSB7XG4gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFN5bWJvbC50b1N0cmluZ1RhZywgeyB2YWx1ZTogJ01vZHVsZScgfSk7XG4gXHRcdH1cbiBcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsICdfX2VzTW9kdWxlJywgeyB2YWx1ZTogdHJ1ZSB9KTtcbiBcdH07XG5cbiBcdC8vIGNyZWF0ZSBhIGZha2UgbmFtZXNwYWNlIG9iamVjdFxuIFx0Ly8gbW9kZSAmIDE6IHZhbHVlIGlzIGEgbW9kdWxlIGlkLCByZXF1aXJlIGl0XG4gXHQvLyBtb2RlICYgMjogbWVyZ2UgYWxsIHByb3BlcnRpZXMgb2YgdmFsdWUgaW50byB0aGUgbnNcbiBcdC8vIG1vZGUgJiA0OiByZXR1cm4gdmFsdWUgd2hlbiBhbHJlYWR5IG5zIG9iamVjdFxuIFx0Ly8gbW9kZSAmIDh8MTogYmVoYXZlIGxpa2UgcmVxdWlyZVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy50ID0gZnVuY3Rpb24odmFsdWUsIG1vZGUpIHtcbiBcdFx0aWYobW9kZSAmIDEpIHZhbHVlID0gX193ZWJwYWNrX3JlcXVpcmVfXyh2YWx1ZSk7XG4gXHRcdGlmKG1vZGUgJiA4KSByZXR1cm4gdmFsdWU7XG4gXHRcdGlmKChtb2RlICYgNCkgJiYgdHlwZW9mIHZhbHVlID09PSAnb2JqZWN0JyAmJiB2YWx1ZSAmJiB2YWx1ZS5fX2VzTW9kdWxlKSByZXR1cm4gdmFsdWU7XG4gXHRcdHZhciBucyA9IE9iamVjdC5jcmVhdGUobnVsbCk7XG4gXHRcdF9fd2VicGFja19yZXF1aXJlX18ucihucyk7XG4gXHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShucywgJ2RlZmF1bHQnLCB7IGVudW1lcmFibGU6IHRydWUsIHZhbHVlOiB2YWx1ZSB9KTtcbiBcdFx0aWYobW9kZSAmIDIgJiYgdHlwZW9mIHZhbHVlICE9ICdzdHJpbmcnKSBmb3IodmFyIGtleSBpbiB2YWx1ZSkgX193ZWJwYWNrX3JlcXVpcmVfXy5kKG5zLCBrZXksIGZ1bmN0aW9uKGtleSkgeyByZXR1cm4gdmFsdWVba2V5XTsgfS5iaW5kKG51bGwsIGtleSkpO1xuIFx0XHRyZXR1cm4gbnM7XG4gXHR9O1xuXG4gXHQvLyBnZXREZWZhdWx0RXhwb3J0IGZ1bmN0aW9uIGZvciBjb21wYXRpYmlsaXR5IHdpdGggbm9uLWhhcm1vbnkgbW9kdWxlc1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5uID0gZnVuY3Rpb24obW9kdWxlKSB7XG4gXHRcdHZhciBnZXR0ZXIgPSBtb2R1bGUgJiYgbW9kdWxlLl9fZXNNb2R1bGUgP1xuIFx0XHRcdGZ1bmN0aW9uIGdldERlZmF1bHQoKSB7IHJldHVybiBtb2R1bGVbJ2RlZmF1bHQnXTsgfSA6XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0TW9kdWxlRXhwb3J0cygpIHsgcmV0dXJuIG1vZHVsZTsgfTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kKGdldHRlciwgJ2EnLCBnZXR0ZXIpO1xuIFx0XHRyZXR1cm4gZ2V0dGVyO1xuIFx0fTtcblxuIFx0Ly8gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm8gPSBmdW5jdGlvbihvYmplY3QsIHByb3BlcnR5KSB7IHJldHVybiBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwob2JqZWN0LCBwcm9wZXJ0eSk7IH07XG5cbiBcdC8vIF9fd2VicGFja19wdWJsaWNfcGF0aF9fXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnAgPSBcIlwiO1xuXG5cbiBcdC8vIExvYWQgZW50cnkgbW9kdWxlIGFuZCByZXR1cm4gZXhwb3J0c1xuIFx0cmV0dXJuIF9fd2VicGFja19yZXF1aXJlX18oX193ZWJwYWNrX3JlcXVpcmVfXy5zID0gXCIuL3NyYy9iYWNrZ3JvdW5kLnRzXCIpO1xuIiwiZXhwb3J0IGNsYXNzIENyYXdsZXIge1xuXG4gICAgLy8gcHJpdmF0ZSBtdXRhdGlvbk9ic2VydmVyOiBNdXRhdGlvbk9ic2VydmVyO1xuXG4gICAgY29uc3RydWN0b3IgKCkge1xuICAgICAgICAvLyB0aGlzLm11dGF0aW9uT2JzZXJ2ZXIgPSBuZXcgTXV0YXRpb25PYnNlcnZlcih0aGlzLm9uTXV0YXRpb24uYmluZCh0aGlzKSk7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogQ2FsbGJhY2sgb24gZG9tIG11dGF0aW9uXG4gICAgICogQHBhcmFtIG11dGF0aW9ucyBcbiAgICAgKi9cbiAgICAvLyBvbk11dGF0aW9uIChtdXRhdGlvbnM6IE11dGF0aW9uUmVjb3JkKSB7XG4gICAgLy8gICAgIGNvbnNvbGUubG9nKG11dGF0aW9ucyk7XG4gICAgLy8gfVxuXHRcblx0ZmluZDxUIGV4dGVuZHMgRWxlbWVudCA9IEVsZW1lbnQ+IChzZWxlY3Rvcjogc3RyaW5nKTogVCB8IG51bGwge1xuXHRcdHJldHVybiBkb2N1bWVudC5xdWVyeVNlbGVjdG9yPFQ+KHNlbGVjdG9yKTtcblx0fVxuXHRcblx0ZmluZElucHV0IChzZWxlY3Rvcjogc3RyaW5nKTogSFRNTElucHV0RWxlbWVudCB8IG51bGwge1xuXHRcdHJldHVybiB0aGlzLmZpbmQ8SFRNTElucHV0RWxlbWVudD4oc2VsZWN0b3IpO1xuXHR9XG5cdFxuXHQvKipcblx0ICogRmluZCBhIGxpbmsgdG8gdGhlIGdpdmVuIHBhdGggZnJvbSB0aGUgY3VycmVudCBwYWdlLiBTaG91bGQgYmUgb3ZlcnJpZGVuIHRvIGJlIG1vcmUgc3BlY2lmaWNcblx0ICogQHBhcmFtIHBhdGggXG5cdCAqL1xuXHRmaW5kTGluayAocGF0aDogc3RyaW5nKTogSFRNTEVsZW1lbnQge1xuXHRcdHJldHVybiB0aGlzLmZpbmQocGF0aCk7XG5cdH1cblxuXHRmaWxsSW5wdXQgKGVsZW1lbnQ6IEhUTUxJbnB1dEVsZW1lbnQsIHRleHQ6IHN0cmluZykge1xuXHRcdGVsZW1lbnQudmFsdWUgPSB0ZXh0O1xuXHR9XG5cbiAgICAvKipcbiAgICAgKiBDbGljayBvbiBhbiBlbGVtZW50XG4gICAgICogQHBhcmFtIGVsZW1lbnQgXG4gICAgICovXG4gICAgYXN5bmMgY2xpY2sgKGVsZW1lbnQ6IEhUTUxFbGVtZW50KTogUHJvbWlzZTx2b2lkPiB7XG4gICAgICAgIC8vIHRoaXMubXV0YXRpb25PYnNlcnZlci5vYnNlcnZlKGRvY3VtZW50LmJvZHksIHtcbiAgICAgICAgLy8gICAgIGF0dHJpYnV0ZXM6IHRydWUsXG4gICAgICAgIC8vICAgICBzdWJ0cmVlOiB0cnVlXG4gICAgICAgIC8vIH0pO1xuICAgICAgICBlbGVtZW50LmNsaWNrKCk7XG5cbiAgICAgICAgLy8gdGhpcy5tdXRhdGlvbk9ic2VydmVyLmRpc2Nvbm5lY3QoKTtcbiAgICB9XG5cbn0iLCJpbXBvcnQgeyBTdXByZW1lIH0gZnJvbSAnLi9zdXByZW1lL1N1cHJlbWUnO1xuXG4vKipcbiAqIExpc3Qgb2YgYXZhaWxhYmxlIHNlbGxlcnNcbiAqL1xuY29uc3QgYWxsU2VsbGVycyA9IFtTdXByZW1lXTtcblxuZnVuY3Rpb24gc2V0dXBTdG9yYWdlICgpIHtcbiAgICBjaHJvbWUuc3RvcmFnZS5zeW5jLnNldCh7XG4gICAgICAgIHRlc3Q6IHRydWVcbiAgICB9LCAoKSA9PiB7XG4gICAgICAgIGNvbnNvbGUubG9nKCdva2F5IHN0b3JhZ2UgaXMgc2V0Jyk7XG4gICAgfSk7XG59XG5cbi8qKlxuICogRW5hYmxlIHBhZ2UgYWN0aW9uIHdoZW4gb24gYSBzZWxsZXIncyB3ZWJzaXRlXG4gKi9cbmZ1bmN0aW9uIHNldHVwRGVjbGFyYXRpdmVDb250ZW50ICgpIHtcbiAgICBjaHJvbWUuZGVjbGFyYXRpdmVDb250ZW50Lm9uUGFnZUNoYW5nZWQucmVtb3ZlUnVsZXModW5kZWZpbmVkLCAoKSA9PiB7XG4gICAgICAgIGNocm9tZS5kZWNsYXJhdGl2ZUNvbnRlbnQub25QYWdlQ2hhbmdlZC5hZGRSdWxlcyhhbGxTZWxsZXJzLm1hcChzZWxsZXIgPT5cbiAgICAgICAgICAgICh7XG4gICAgICAgICAgICAgICAgY29uZGl0aW9uczogW1xuICAgICAgICAgICAgICAgICAgICBuZXcgY2hyb21lLmRlY2xhcmF0aXZlQ29udGVudC5QYWdlU3RhdGVNYXRjaGVyKHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHBhZ2VVcmw6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBob3N0RXF1YWxzOiBzZWxsZXIuaG9zdG5hbWVcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgICAgICBdLFxuICAgICAgICAgICAgICAgIGFjdGlvbnM6IFtcbiAgICAgICAgICAgICAgICAgICAgbmV3IGNocm9tZS5kZWNsYXJhdGl2ZUNvbnRlbnQuU2hvd1BhZ2VBY3Rpb24oKVxuICAgICAgICAgICAgICAgIF1cbiAgICAgICAgICAgIH0pXG4gICAgICAgICkpO1xuICAgIH0pO1xufVxuXG5jaHJvbWUucnVudGltZS5vbkluc3RhbGxlZC5hZGRMaXN0ZW5lcihfID0+IHtcbiAgICBzZXR1cFN0b3JhZ2UoKTtcbiAgICBzZXR1cERlY2xhcmF0aXZlQ29udGVudCgpO1xufSk7XG4iLCJpbXBvcnQgeyBJU2VsbGVyIH0gZnJvbSAnLi4vSVNlbGxlcic7XG5pbXBvcnQgeyBDcmF3bGVyIH0gZnJvbSAnLi4vQ3Jhd2xlcic7XG5pbXBvcnQgeyBJQ2hlY2tvdXRJbmZvcm1hdGlvbiB9IGZyb20gJy4vSUNoZWNrb3V0SW5mb3JtYXRpb24nO1xuaW1wb3J0IHsgSUNoZWNrb3V0Rm9ybSB9IGZyb20gJy4vSUNoZWNrb3V0Rm9ybSc7XG5cbi8vIElERUEgcGx1Z2luIHN5c3RlbSwgYWRkIG5ldyBjbGFzc2VzLiBEZWZpbmUgSW50ZXJmYWNlcyAoVFMpXG5cbmV4cG9ydCBjbGFzcyBTdXByZW1lIGV4dGVuZHMgQ3Jhd2xlciBpbXBsZW1lbnRzIElTZWxsZXIge1xuXG5cdGZpbmRMaW5rIChwYXRoOiBzdHJpbmcpOiBIVE1MRWxlbWVudCB7XG5cdFx0cmV0dXJuIHRoaXMuZmluZChwYXRoKVxuICAgICAgICB8fCB0aGlzLmZpbmQoYGh0dHA6Ly8ke1N1cHJlbWUuaG9zdG5hbWV9JHtwYXRofWApXG4gICAgICAgIHx8IHRoaXMuZmluZChgaHR0cHM6Ly8ke1N1cHJlbWUuaG9zdG5hbWV9JHtwYXRofWApO1xuXHR9XG5cbiAgICBnZXRQcm9kdWN0cyAoKSB7XG4gICAgICAgIGZvciAoY29uc3QgY2F0ZWdvcnkgb2YgU3VwcmVtZS5jYXRlZ29yaWVzKSB7XG4gICAgICAgICAgICBjb25zdCBsaW5rID0gdGhpcy5maW5kTGluayhgL3Nob3AvYWxsLyR7Y2F0ZWdvcnl9YCk7XG4gICAgICAgICAgICBpZiAoIWxpbmspIGNvbnRpbnVlO1xuICAgICAgICAgICAgdGhpcy5jbGljayhsaW5rKTtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gW107XG4gICAgfVxuXG5cdC8qKlxuXHQgKiBHZXQgY2hlY2tvdXQgaW5mb3JtYXRpb24gKGRhdGEgbmVlZGVkIGJ5IHRoZSBmb3JtKVxuXHQgKi9cblx0Z2V0Q2hlY2tvdXRJbmZvcm1hdGlvbiAoKTogSUNoZWNrb3V0SW5mb3JtYXRpb24ge1xuXHRcdHJldHVybiB7XG5cdFx0XHRuYW1lOiAnSm9obiBEb2UnLFxuXHRcdFx0ZW1haWw6ICdkdW1teUBlbWFpbC5sb2wnLFxuXHRcdFx0cGhvbmU6ICcwMDAwMDAwMDAwJyxcblx0XHRcdGFkZHJlc3M6ICczMzAgYWxsw6llIGRlcyBVcnN1bGluZXMnLFxuXHRcdFx0YXBwdDogJzMwMicsXG5cdFx0XHR6aXBjb2RlOiAnRzVMIDJaOScsXG5cdFx0XHRjaXR5OiAnUmltb3Vza2knLFxuXHRcdFx0Y291bnRyeTogJ0NBTkFEQScsXG5cdFx0XHRzdGF0ZTogJ1FDJyxcblx0XHRcdG51bWJlcjogJzUxMDAwMDAwMDAwMDAwMTYnLFxuXHRcdFx0ZXhwTW9udGg6ICcxMicsXG5cdFx0XHRleHBZZWFyOiAnMjAyMScsXG5cdFx0XHRjdnY6ICcxMjMnLFxuXHRcdH07XG5cdFx0Ly8gTXVzdCBzZXQgY291bnRyeSBmaXJzdCwgdGhlbiBzdGF0ZVxuXHR9XG5cblx0LyoqXG5cdCAqIEdldCBjaGVja291dCBmb3JtIGVsZW1lbnRzXG5cdCAqL1xuXHRnZXRDaGVja291dEZvcm0gKCk6IElDaGVja291dEZvcm0ge1xuXHRcdGxldCBlbGVtZW50czogSUNoZWNrb3V0Rm9ybSA9IHt9IGFzIElDaGVja291dEZvcm07XG5cblx0XHRmb3IgKGNvbnN0IFtrZXksIHNlbGVjdG9yXSBvZiBPYmplY3QuZW50cmllcyhTdXByZW1lLmNoZWNrb3V0U2VsZWN0b3JzKSkge1xuXHRcdFx0ZWxlbWVudHNba2V5XSA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3Ioc2VsZWN0b3IpO1xuXHRcdH1cblx0XHRyZXR1cm4gZWxlbWVudHM7XG5cdH1cblxuXHQvKipcblx0ICogRmlsbCBjaGVja291dCBpbmZvcm1hdGlvbiBhbmQgcHJvY2VlZC5cblx0ICogTmVlZHMgdG8gYWxyZWFkeSBiZSBvbiBjaGVja291dCBwYWdlLlxuXHQgKi9cblx0Y2hlY2tvdXQgKCkge1xuXHRcdGxldCBkYXRhOiBJQ2hlY2tvdXRJbmZvcm1hdGlvbiA9IHRoaXMuZ2V0Q2hlY2tvdXRJbmZvcm1hdGlvbigpO1xuXHRcdGxldCBlbGVtZW50czogSUNoZWNrb3V0Rm9ybSA9IHRoaXMuZ2V0Q2hlY2tvdXRGb3JtKCk7XG5cdFx0XG5cdFx0dHJ5IHtcblx0XHRcdC8vIEZpbGwgcmVndWxhciBpbnB1dFxuXHRcdFx0Zm9yIChjb25zdCBrZXkgaW4gZGF0YSkge1xuXHRcdFx0XHR0aGlzLmZpbGxJbnB1dChlbGVtZW50c1trZXldLCBkYXRhW2tleV0pO1xuXHRcdFx0fVxuXHRcdFx0Ly8gQ2hlY2sgVE9TIGNoZWNrYm94XG5cdFx0XHRlbGVtZW50cy50b3MuY2hlY2tlZCA9IHRydWU7XG5cdFx0XHQvLyBTdWJtaXQgZm9ybVxuXHRcdFx0dGhpcy5jbGljayhlbGVtZW50cy5zdWJtaXQpO1xuXHRcdFx0Ly8gY29uc3QgZSA9IHRoaXMuZmluZElucHV0KFN1cHJlbWUuY2hlY2tvdXRTZWxlY3RvcnMubmFtZSk7XG5cdFx0XHQvLyB0aGlzLmZpbGxJbnB1dChlLCAnbG9sJyk7XG5cdFx0fSBjYXRjaCAoZXJyKSB7XG5cdFx0XHRjb25zb2xlLmxvZyhlcnIpO1xuXHRcdH1cblx0fVxuXG4gICAgLyoqXG4gICAgICogV2Vic2l0ZSBob3N0bmFtZVxuICAgICAqL1xuICAgIHN0YXRpYyBnZXQgaG9zdG5hbWUgKCkge1xuICAgICAgICByZXR1cm4gJ3d3dy5zdXByZW1lbmV3eW9yay5jb20nO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIEF2YWlsYWJsZSBwcm9kdWN0IGNhdGVnb3JpZXNcbiAgICAgKi9cbiAgICBzdGF0aWMgZ2V0IGNhdGVnb3JpZXMgKCk6IHN0cmluZ1tdIHtcbiAgICAgICAgcmV0dXJuIFtcbiAgICAgICAgICAgICdqYWNrZXRzJyxcbiAgICAgICAgICAgICdzaGlydHMnLFxuICAgICAgICAgICAgJ3RvcHMvc3dlYXRlcnMnLFxuICAgICAgICAgICAgJ3N3ZWF0c2hpcnRzJyxcbiAgICAgICAgICAgICdwYW50cycsXG4gICAgICAgICAgICAnaGF0cycsXG4gICAgICAgICAgICAnYmFncycsXG4gICAgICAgICAgICAnYWNjZXNzb3JpZXMnLFxuICAgICAgICAgICAgJ3NrYXRlJ1xuICAgICAgICBdO1xuXHR9XG5cdFxuXHQvKipcblx0ICogQ1NTIHNlbGVjdG9ycyBvbiB0aGUgY2hlY2tvdXQgcGFnZVxuXHQgKiBOT1RFOiBvbmx5IG9uIFVTIHZlcnNpb24gb25seVxuXHQgKi9cblx0c3RhdGljIGdldCBjaGVja291dFNlbGVjdG9ycyAoKSB7XG5cdFx0cmV0dXJuIHtcblx0XHRcdG5hbWU6ICcjb3JkZXJfYmlsbGluZ19uYW1lJyxcblx0XHRcdGVtYWlsOiAnI29yZGVyX2VtYWlsJyxcblx0XHRcdHBob25lOiAnI29yZGVyX3RlbCcsXG5cdFx0XHRhZGRyZXNzOiAnI2JvJyxcblx0XHRcdGFwcHQ6ICcjb2JhMycsXG5cdFx0XHR6aXBjb2RlOiAnI29yZGVyX2JpbGxpbmdfemlwJyxcblx0XHRcdGNpdHk6ICcjb3JkZXJfYmlsbGluZ19jaXR5Jyxcblx0XHRcdGNvdW50cnk6ICcjb3JkZXJfYmlsbGluZ19jb3VudHJ5Jyxcblx0XHRcdHN0YXRlOiAnI29yZGVyX2JpbGxpbmdfc3RhdGUnLFxuXHRcdFx0bnVtYmVyOiAnI25uYWVyYicsXG5cdFx0XHRleHBNb250aDogJyNjcmVkaXRfY2FyZF9tb250aCcsXG5cdFx0XHRleHBZZWFyOiAnI2NyZWRpdF9jYXJkX3llYXInLFxuXHRcdFx0Y3Z2OiAnI29yY2VyJyxcblx0XHRcdHRvczogJyNvcmRlcl90ZXJtcycsXG5cdFx0XHRzdWJtaXQ6ICcjcGF5ID4gaW5wdXQnXG5cdFx0fTtcblx0fVxuXG4gICAgLyoqXG4gICAgICogTmF2aWdhdGlvbiB1cmxzXG4gICAgICovXG4gICAgc3RhdGljIGdldCBVUkxTICgpIHtcbiAgICAgICAgcmV0dXJuIHtcbiAgICAgICAgICAgIEJBU0U6ICcvJyxcbiAgICAgICAgICAgIFNIT1A6IHtcbiAgICAgICAgICAgICAgICBCQVNFOiAnL3Nob3AnLFxuICAgICAgICAgICAgICAgIEFMTDogJy9zaG9wL2FsbCcsXG4gICAgICAgICAgICAgICAgTkVXOiAnL3Nob3AvbmV3JyxcbiAgICAgICAgICAgICAgICBKQUNLRVRTOiAnL3Nob3AvamFja2V0cycsXG4gICAgICAgICAgICAgICAgU0hJUlRTOiAnL3Nob3Avc2hpcnRzJyxcbiAgICAgICAgICAgICAgICBUT1BTX1NXRUFURVJTOiAnL3Nob3AvdG9wc19zd2VhdGVycycsXG4gICAgICAgICAgICAgICAgU1dFQVRTSElSVFM6ICcvc2hvcC9zd2VhdHNoaXJ0cycsXG4gICAgICAgICAgICAgICAgUEFOVFM6ICcvc2hvcC9wYW50cycsXG4gICAgICAgICAgICAgICAgSEFUUzogJy9zaG9wL2hhdHMnLFxuICAgICAgICAgICAgICAgIEJBR1M6ICcvc2hvcC9iYWdzJyxcbiAgICAgICAgICAgICAgICBBQ0NFU1NPUklFUzogJy9zaG9wL2FjY2Vzc29yaWVzJyxcbiAgICAgICAgICAgICAgICBTS0FURVM6ICcvc2hvcC9za2F0ZScsXG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgQ0FSVDogJy9jYXJ0JyxcbiAgICAgICAgICAgIENIRUNLT1VUOiAnL2NoZWNrb3V0JyxcbiAgICAgICAgfTtcbiAgICB9XG5cbn1cbiJdLCJzb3VyY2VSb290IjoiIn0=