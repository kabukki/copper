/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/supreme/index.ts");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/Crawler.ts":
/*!************************!*\
  !*** ./src/Crawler.ts ***!
  \************************/
/*! exports provided: Crawler */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Crawler", function() { return Crawler; });
class Crawler {
    // private mutationObserver: MutationObserver;
    constructor() {
        // this.mutationObserver = new MutationObserver(this.onMutation.bind(this));
    }
    /**
     * Callback on dom mutation
     * @param mutations
     */
    // onMutation (mutations: MutationRecord) {
    //     console.log(mutations);
    // }
    find(selector) {
        return document.querySelector(selector);
    }
    findInput(selector) {
        return this.find(selector);
    }
    /**
     * Find a link to the given path from the current page. Should be overriden to be more specific
     * @param path
     */
    findLink(path) {
        return this.find(path);
    }
    fillInput(element, text) {
        element.value = text;
    }
    /**
     * Click on an element
     * @param element
     */
    async click(element) {
        // this.mutationObserver.observe(document.body, {
        //     attributes: true,
        //     subtree: true
        // });
        element.click();
        // this.mutationObserver.disconnect();
    }
}


/***/ }),

/***/ "./src/supreme/Supreme.ts":
/*!********************************!*\
  !*** ./src/supreme/Supreme.ts ***!
  \********************************/
/*! exports provided: Supreme */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Supreme", function() { return Supreme; });
/* harmony import */ var _Crawler__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../Crawler */ "./src/Crawler.ts");

// IDEA plugin system, add new classes. Define Interfaces (TS)
class Supreme extends _Crawler__WEBPACK_IMPORTED_MODULE_0__["Crawler"] {
    findLink(path) {
        return this.find(path)
            || this.find(`http://${Supreme.hostname}${path}`)
            || this.find(`https://${Supreme.hostname}${path}`);
    }
    getProducts() {
        for (const category of Supreme.categories) {
            const link = this.findLink(`/shop/all/${category}`);
            if (!link)
                continue;
            this.click(link);
        }
        return [];
    }
    /**
     * Get checkout information (data needed by the form)
     */
    getCheckoutInformation() {
        return {
            name: 'John Doe',
            email: 'dummy@email.lol',
            phone: '0000000000',
            address: '330 allée des Ursulines',
            appt: '302',
            zipcode: 'G5L 2Z9',
            city: 'Rimouski',
            country: 'CANADA',
            state: 'QC',
            number: '5100000000000016',
            expMonth: '12',
            expYear: '2021',
            cvv: '123',
        };
        // Must set country first, then state
    }
    /**
     * Get checkout form elements
     */
    getCheckoutForm() {
        let elements = {};
        for (const [key, selector] of Object.entries(Supreme.checkoutSelectors)) {
            elements[key] = document.querySelector(selector);
        }
        return elements;
    }
    /**
     * Fill checkout information and proceed.
     * Needs to already be on checkout page.
     */
    checkout() {
        let data = this.getCheckoutInformation();
        let elements = this.getCheckoutForm();
        try {
            // Fill regular input
            for (const key in data) {
                this.fillInput(elements[key], data[key]);
            }
            // Check TOS checkbox
            elements.tos.checked = true;
            // Submit form
            this.click(elements.submit);
            // const e = this.findInput(Supreme.checkoutSelectors.name);
            // this.fillInput(e, 'lol');
        }
        catch (err) {
            console.log(err);
        }
    }
    /**
     * Website hostname
     */
    static get hostname() {
        return 'www.supremenewyork.com';
    }
    /**
     * Available product categories
     */
    static get categories() {
        return [
            'jackets',
            'shirts',
            'tops/sweaters',
            'sweatshirts',
            'pants',
            'hats',
            'bags',
            'accessories',
            'skate'
        ];
    }
    /**
     * CSS selectors on the checkout page
     * NOTE: only on US version only
     */
    static get checkoutSelectors() {
        return {
            name: '#order_billing_name',
            email: '#order_email',
            phone: '#order_tel',
            address: '#bo',
            appt: '#oba3',
            zipcode: '#order_billing_zip',
            city: '#order_billing_city',
            country: '#order_billing_country',
            state: '#order_billing_state',
            number: '#nnaerb',
            expMonth: '#credit_card_month',
            expYear: '#credit_card_year',
            cvv: '#orcer',
            tos: '#order_terms',
            submit: '#pay > input'
        };
    }
    /**
     * Navigation urls
     */
    static get URLS() {
        return {
            BASE: '/',
            SHOP: {
                BASE: '/shop',
                ALL: '/shop/all',
                NEW: '/shop/new',
                JACKETS: '/shop/jackets',
                SHIRTS: '/shop/shirts',
                TOPS_SWEATERS: '/shop/tops_sweaters',
                SWEATSHIRTS: '/shop/sweatshirts',
                PANTS: '/shop/pants',
                HATS: '/shop/hats',
                BAGS: '/shop/bags',
                ACCESSORIES: '/shop/accessories',
                SKATES: '/shop/skate',
            },
            CART: '/cart',
            CHECKOUT: '/checkout',
        };
    }
}


/***/ }),

/***/ "./src/supreme/index.ts":
/*!******************************!*\
  !*** ./src/supreme/index.ts ***!
  \******************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Supreme__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Supreme */ "./src/supreme/Supreme.ts");
/**
 * This script is intended to be run in the context of the Supreme online shop.
 */

let supreme = new _Supreme__WEBPACK_IMPORTED_MODULE_0__["Supreme"]();
function goToShop() {
    if (!window.location.href.includes(_Supreme__WEBPACK_IMPORTED_MODULE_0__["Supreme"].URLS.SHOP.BASE)) {
        const shopLink = supreme.findLink(_Supreme__WEBPACK_IMPORTED_MODULE_0__["Supreme"].URLS.SHOP.BASE);
        if (shopLink) {
            shopLink.click();
        }
        else {
            throw new Error('Cannot find a link to the shop. Aborting');
        }
    }
    else {
        console.warn('Already on shop page');
    }
}
try {
    // goToShop();
    supreme.checkout();
}
catch (err) {
    console.error(err);
}
// setInterval(_ => {
//     console.log(window.location.href);
// }, 1000)


/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vLy4vc3JjL0NyYXdsZXIudHMiLCJ3ZWJwYWNrOi8vLy4vc3JjL3N1cHJlbWUvU3VwcmVtZS50cyIsIndlYnBhY2s6Ly8vLi9zcmMvc3VwcmVtZS9pbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxrREFBMEMsZ0NBQWdDO0FBQzFFO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsZ0VBQXdELGtCQUFrQjtBQUMxRTtBQUNBLHlEQUFpRCxjQUFjO0FBQy9EOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpREFBeUMsaUNBQWlDO0FBQzFFLHdIQUFnSCxtQkFBbUIsRUFBRTtBQUNySTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLG1DQUEyQiwwQkFBMEIsRUFBRTtBQUN2RCx5Q0FBaUMsZUFBZTtBQUNoRDtBQUNBO0FBQ0E7O0FBRUE7QUFDQSw4REFBc0QsK0RBQStEOztBQUVySDtBQUNBOzs7QUFHQTtBQUNBOzs7Ozs7Ozs7Ozs7O0FDbEZBO0FBQUE7QUFBTyxNQUFNLE9BQU87SUFFaEIsOENBQThDO0lBRTlDO1FBQ0ksNEVBQTRFO0lBQ2hGLENBQUM7SUFFRDs7O09BR0c7SUFDSCwyQ0FBMkM7SUFDM0MsOEJBQThCO0lBQzlCLElBQUk7SUFFUCxJQUFJLENBQStCLFFBQWdCO1FBQ2xELE9BQU8sUUFBUSxDQUFDLGFBQWEsQ0FBSSxRQUFRLENBQUMsQ0FBQztJQUM1QyxDQUFDO0lBRUQsU0FBUyxDQUFFLFFBQWdCO1FBQzFCLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBbUIsUUFBUSxDQUFDLENBQUM7SUFDOUMsQ0FBQztJQUVEOzs7T0FHRztJQUNILFFBQVEsQ0FBRSxJQUFZO1FBQ3JCLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUN4QixDQUFDO0lBRUQsU0FBUyxDQUFFLE9BQXlCLEVBQUUsSUFBWTtRQUNqRCxPQUFPLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQztJQUN0QixDQUFDO0lBRUU7OztPQUdHO0lBQ0gsS0FBSyxDQUFDLEtBQUssQ0FBRSxPQUFvQjtRQUM3QixpREFBaUQ7UUFDakQsd0JBQXdCO1FBQ3hCLG9CQUFvQjtRQUNwQixNQUFNO1FBQ04sT0FBTyxDQUFDLEtBQUssRUFBRSxDQUFDO1FBRWhCLHNDQUFzQztJQUMxQyxDQUFDO0NBRUo7Ozs7Ozs7Ozs7Ozs7QUNqREQ7QUFBQTtBQUFBO0FBQXFDO0FBSXJDLDhEQUE4RDtBQUV2RCxNQUFNLE9BQVEsU0FBUSxnREFBTztJQUVuQyxRQUFRLENBQUUsSUFBWTtRQUNyQixPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO2VBQ2IsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLE9BQU8sQ0FBQyxRQUFRLEdBQUcsSUFBSSxFQUFFLENBQUM7ZUFDOUMsSUFBSSxDQUFDLElBQUksQ0FBQyxXQUFXLE9BQU8sQ0FBQyxRQUFRLEdBQUcsSUFBSSxFQUFFLENBQUMsQ0FBQztJQUMxRCxDQUFDO0lBRUUsV0FBVztRQUNQLEtBQUssTUFBTSxRQUFRLElBQUksT0FBTyxDQUFDLFVBQVUsRUFBRTtZQUN2QyxNQUFNLElBQUksR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLGFBQWEsUUFBUSxFQUFFLENBQUMsQ0FBQztZQUNwRCxJQUFJLENBQUMsSUFBSTtnQkFBRSxTQUFTO1lBQ3BCLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUM7U0FDcEI7UUFDRCxPQUFPLEVBQUUsQ0FBQztJQUNkLENBQUM7SUFFSjs7T0FFRztJQUNILHNCQUFzQjtRQUNyQixPQUFPO1lBQ04sSUFBSSxFQUFFLFVBQVU7WUFDaEIsS0FBSyxFQUFFLGlCQUFpQjtZQUN4QixLQUFLLEVBQUUsWUFBWTtZQUNuQixPQUFPLEVBQUUseUJBQXlCO1lBQ2xDLElBQUksRUFBRSxLQUFLO1lBQ1gsT0FBTyxFQUFFLFNBQVM7WUFDbEIsSUFBSSxFQUFFLFVBQVU7WUFDaEIsT0FBTyxFQUFFLFFBQVE7WUFDakIsS0FBSyxFQUFFLElBQUk7WUFDWCxNQUFNLEVBQUUsa0JBQWtCO1lBQzFCLFFBQVEsRUFBRSxJQUFJO1lBQ2QsT0FBTyxFQUFFLE1BQU07WUFDZixHQUFHLEVBQUUsS0FBSztTQUNWLENBQUM7UUFDRixxQ0FBcUM7SUFDdEMsQ0FBQztJQUVEOztPQUVHO0lBQ0gsZUFBZTtRQUNkLElBQUksUUFBUSxHQUFrQixFQUFtQixDQUFDO1FBRWxELEtBQUssTUFBTSxDQUFDLEdBQUcsRUFBRSxRQUFRLENBQUMsSUFBSSxNQUFNLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxpQkFBaUIsQ0FBQyxFQUFFO1lBQ3hFLFFBQVEsQ0FBQyxHQUFHLENBQUMsR0FBRyxRQUFRLENBQUMsYUFBYSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1NBQ2pEO1FBQ0QsT0FBTyxRQUFRLENBQUM7SUFDakIsQ0FBQztJQUVEOzs7T0FHRztJQUNILFFBQVE7UUFDUCxJQUFJLElBQUksR0FBeUIsSUFBSSxDQUFDLHNCQUFzQixFQUFFLENBQUM7UUFDL0QsSUFBSSxRQUFRLEdBQWtCLElBQUksQ0FBQyxlQUFlLEVBQUUsQ0FBQztRQUVyRCxJQUFJO1lBQ0gscUJBQXFCO1lBQ3JCLEtBQUssTUFBTSxHQUFHLElBQUksSUFBSSxFQUFFO2dCQUN2QixJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsRUFBRSxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQzthQUN6QztZQUNELHFCQUFxQjtZQUNyQixRQUFRLENBQUMsR0FBRyxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUM7WUFDNUIsY0FBYztZQUNkLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1lBQzVCLDREQUE0RDtZQUM1RCw0QkFBNEI7U0FDNUI7UUFBQyxPQUFPLEdBQUcsRUFBRTtZQUNiLE9BQU8sQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUM7U0FDakI7SUFDRixDQUFDO0lBRUU7O09BRUc7SUFDSCxNQUFNLEtBQUssUUFBUTtRQUNmLE9BQU8sd0JBQXdCLENBQUM7SUFDcEMsQ0FBQztJQUVEOztPQUVHO0lBQ0gsTUFBTSxLQUFLLFVBQVU7UUFDakIsT0FBTztZQUNILFNBQVM7WUFDVCxRQUFRO1lBQ1IsZUFBZTtZQUNmLGFBQWE7WUFDYixPQUFPO1lBQ1AsTUFBTTtZQUNOLE1BQU07WUFDTixhQUFhO1lBQ2IsT0FBTztTQUNWLENBQUM7SUFDVCxDQUFDO0lBRUQ7OztPQUdHO0lBQ0gsTUFBTSxLQUFLLGlCQUFpQjtRQUMzQixPQUFPO1lBQ04sSUFBSSxFQUFFLHFCQUFxQjtZQUMzQixLQUFLLEVBQUUsY0FBYztZQUNyQixLQUFLLEVBQUUsWUFBWTtZQUNuQixPQUFPLEVBQUUsS0FBSztZQUNkLElBQUksRUFBRSxPQUFPO1lBQ2IsT0FBTyxFQUFFLG9CQUFvQjtZQUM3QixJQUFJLEVBQUUscUJBQXFCO1lBQzNCLE9BQU8sRUFBRSx3QkFBd0I7WUFDakMsS0FBSyxFQUFFLHNCQUFzQjtZQUM3QixNQUFNLEVBQUUsU0FBUztZQUNqQixRQUFRLEVBQUUsb0JBQW9CO1lBQzlCLE9BQU8sRUFBRSxtQkFBbUI7WUFDNUIsR0FBRyxFQUFFLFFBQVE7WUFDYixHQUFHLEVBQUUsY0FBYztZQUNuQixNQUFNLEVBQUUsY0FBYztTQUN0QixDQUFDO0lBQ0gsQ0FBQztJQUVFOztPQUVHO0lBQ0gsTUFBTSxLQUFLLElBQUk7UUFDWCxPQUFPO1lBQ0gsSUFBSSxFQUFFLEdBQUc7WUFDVCxJQUFJLEVBQUU7Z0JBQ0YsSUFBSSxFQUFFLE9BQU87Z0JBQ2IsR0FBRyxFQUFFLFdBQVc7Z0JBQ2hCLEdBQUcsRUFBRSxXQUFXO2dCQUNoQixPQUFPLEVBQUUsZUFBZTtnQkFDeEIsTUFBTSxFQUFFLGNBQWM7Z0JBQ3RCLGFBQWEsRUFBRSxxQkFBcUI7Z0JBQ3BDLFdBQVcsRUFBRSxtQkFBbUI7Z0JBQ2hDLEtBQUssRUFBRSxhQUFhO2dCQUNwQixJQUFJLEVBQUUsWUFBWTtnQkFDbEIsSUFBSSxFQUFFLFlBQVk7Z0JBQ2xCLFdBQVcsRUFBRSxtQkFBbUI7Z0JBQ2hDLE1BQU0sRUFBRSxhQUFhO2FBQ3hCO1lBQ0QsSUFBSSxFQUFFLE9BQU87WUFDYixRQUFRLEVBQUUsV0FBVztTQUN4QixDQUFDO0lBQ04sQ0FBQztDQUVKOzs7Ozs7Ozs7Ozs7O0FDM0pEO0FBQUE7QUFBQTs7R0FFRztBQUVpQztBQUVwQyxJQUFJLE9BQU8sR0FBRyxJQUFJLGdEQUFPLEVBQUUsQ0FBQztBQUU1QixTQUFTLFFBQVE7SUFDaEIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxnREFBTyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEVBQUU7UUFDM0QsTUFBTSxRQUFRLEdBQUcsT0FBTyxDQUFDLFFBQVEsQ0FBQyxnREFBTyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDMUQsSUFBSSxRQUFRLEVBQUU7WUFDYixRQUFRLENBQUMsS0FBSyxFQUFFLENBQUM7U0FDakI7YUFBTTtZQUNOLE1BQU0sSUFBSSxLQUFLLENBQUMsMENBQTBDLENBQUMsQ0FBQztTQUM1RDtLQUNEO1NBQU07UUFDTixPQUFPLENBQUMsSUFBSSxDQUFDLHNCQUFzQixDQUFDLENBQUM7S0FDckM7QUFDRixDQUFDO0FBRUQsSUFBSTtJQUNILGNBQWM7SUFDZCxPQUFPLENBQUMsUUFBUSxFQUFFLENBQUM7Q0FDbkI7QUFBQyxPQUFPLEdBQUcsRUFBRTtJQUNiLE9BQU8sQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUM7Q0FDbkI7QUFFRCxxQkFBcUI7QUFDckIseUNBQXlDO0FBQ3pDLFdBQVciLCJmaWxlIjoic3VwcmVtZS5qcyIsInNvdXJjZXNDb250ZW50IjpbIiBcdC8vIFRoZSBtb2R1bGUgY2FjaGVcbiBcdHZhciBpbnN0YWxsZWRNb2R1bGVzID0ge307XG5cbiBcdC8vIFRoZSByZXF1aXJlIGZ1bmN0aW9uXG4gXHRmdW5jdGlvbiBfX3dlYnBhY2tfcmVxdWlyZV9fKG1vZHVsZUlkKSB7XG5cbiBcdFx0Ly8gQ2hlY2sgaWYgbW9kdWxlIGlzIGluIGNhY2hlXG4gXHRcdGlmKGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdKSB7XG4gXHRcdFx0cmV0dXJuIGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdLmV4cG9ydHM7XG4gXHRcdH1cbiBcdFx0Ly8gQ3JlYXRlIGEgbmV3IG1vZHVsZSAoYW5kIHB1dCBpdCBpbnRvIHRoZSBjYWNoZSlcbiBcdFx0dmFyIG1vZHVsZSA9IGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdID0ge1xuIFx0XHRcdGk6IG1vZHVsZUlkLFxuIFx0XHRcdGw6IGZhbHNlLFxuIFx0XHRcdGV4cG9ydHM6IHt9XG4gXHRcdH07XG5cbiBcdFx0Ly8gRXhlY3V0ZSB0aGUgbW9kdWxlIGZ1bmN0aW9uXG4gXHRcdG1vZHVsZXNbbW9kdWxlSWRdLmNhbGwobW9kdWxlLmV4cG9ydHMsIG1vZHVsZSwgbW9kdWxlLmV4cG9ydHMsIF9fd2VicGFja19yZXF1aXJlX18pO1xuXG4gXHRcdC8vIEZsYWcgdGhlIG1vZHVsZSBhcyBsb2FkZWRcbiBcdFx0bW9kdWxlLmwgPSB0cnVlO1xuXG4gXHRcdC8vIFJldHVybiB0aGUgZXhwb3J0cyBvZiB0aGUgbW9kdWxlXG4gXHRcdHJldHVybiBtb2R1bGUuZXhwb3J0cztcbiBcdH1cblxuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZXMgb2JqZWN0IChfX3dlYnBhY2tfbW9kdWxlc19fKVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5tID0gbW9kdWxlcztcblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGUgY2FjaGVcbiBcdF9fd2VicGFja19yZXF1aXJlX18uYyA9IGluc3RhbGxlZE1vZHVsZXM7XG5cbiBcdC8vIGRlZmluZSBnZXR0ZXIgZnVuY3Rpb24gZm9yIGhhcm1vbnkgZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kID0gZnVuY3Rpb24oZXhwb3J0cywgbmFtZSwgZ2V0dGVyKSB7XG4gXHRcdGlmKCFfX3dlYnBhY2tfcmVxdWlyZV9fLm8oZXhwb3J0cywgbmFtZSkpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgbmFtZSwgeyBlbnVtZXJhYmxlOiB0cnVlLCBnZXQ6IGdldHRlciB9KTtcbiBcdFx0fVxuIFx0fTtcblxuIFx0Ly8gZGVmaW5lIF9fZXNNb2R1bGUgb24gZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5yID0gZnVuY3Rpb24oZXhwb3J0cykge1xuIFx0XHRpZih0eXBlb2YgU3ltYm9sICE9PSAndW5kZWZpbmVkJyAmJiBTeW1ib2wudG9TdHJpbmdUYWcpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgU3ltYm9sLnRvU3RyaW5nVGFnLCB7IHZhbHVlOiAnTW9kdWxlJyB9KTtcbiBcdFx0fVxuIFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgJ19fZXNNb2R1bGUnLCB7IHZhbHVlOiB0cnVlIH0pO1xuIFx0fTtcblxuIFx0Ly8gY3JlYXRlIGEgZmFrZSBuYW1lc3BhY2Ugb2JqZWN0XG4gXHQvLyBtb2RlICYgMTogdmFsdWUgaXMgYSBtb2R1bGUgaWQsIHJlcXVpcmUgaXRcbiBcdC8vIG1vZGUgJiAyOiBtZXJnZSBhbGwgcHJvcGVydGllcyBvZiB2YWx1ZSBpbnRvIHRoZSBuc1xuIFx0Ly8gbW9kZSAmIDQ6IHJldHVybiB2YWx1ZSB3aGVuIGFscmVhZHkgbnMgb2JqZWN0XG4gXHQvLyBtb2RlICYgOHwxOiBiZWhhdmUgbGlrZSByZXF1aXJlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnQgPSBmdW5jdGlvbih2YWx1ZSwgbW9kZSkge1xuIFx0XHRpZihtb2RlICYgMSkgdmFsdWUgPSBfX3dlYnBhY2tfcmVxdWlyZV9fKHZhbHVlKTtcbiBcdFx0aWYobW9kZSAmIDgpIHJldHVybiB2YWx1ZTtcbiBcdFx0aWYoKG1vZGUgJiA0KSAmJiB0eXBlb2YgdmFsdWUgPT09ICdvYmplY3QnICYmIHZhbHVlICYmIHZhbHVlLl9fZXNNb2R1bGUpIHJldHVybiB2YWx1ZTtcbiBcdFx0dmFyIG5zID0gT2JqZWN0LmNyZWF0ZShudWxsKTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5yKG5zKTtcbiBcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KG5zLCAnZGVmYXVsdCcsIHsgZW51bWVyYWJsZTogdHJ1ZSwgdmFsdWU6IHZhbHVlIH0pO1xuIFx0XHRpZihtb2RlICYgMiAmJiB0eXBlb2YgdmFsdWUgIT0gJ3N0cmluZycpIGZvcih2YXIga2V5IGluIHZhbHVlKSBfX3dlYnBhY2tfcmVxdWlyZV9fLmQobnMsIGtleSwgZnVuY3Rpb24oa2V5KSB7IHJldHVybiB2YWx1ZVtrZXldOyB9LmJpbmQobnVsbCwga2V5KSk7XG4gXHRcdHJldHVybiBucztcbiBcdH07XG5cbiBcdC8vIGdldERlZmF1bHRFeHBvcnQgZnVuY3Rpb24gZm9yIGNvbXBhdGliaWxpdHkgd2l0aCBub24taGFybW9ueSBtb2R1bGVzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm4gPSBmdW5jdGlvbihtb2R1bGUpIHtcbiBcdFx0dmFyIGdldHRlciA9IG1vZHVsZSAmJiBtb2R1bGUuX19lc01vZHVsZSA/XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0RGVmYXVsdCgpIHsgcmV0dXJuIG1vZHVsZVsnZGVmYXVsdCddOyB9IDpcbiBcdFx0XHRmdW5jdGlvbiBnZXRNb2R1bGVFeHBvcnRzKCkgeyByZXR1cm4gbW9kdWxlOyB9O1xuIFx0XHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQoZ2V0dGVyLCAnYScsIGdldHRlcik7XG4gXHRcdHJldHVybiBnZXR0ZXI7XG4gXHR9O1xuXG4gXHQvLyBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGxcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubyA9IGZ1bmN0aW9uKG9iamVjdCwgcHJvcGVydHkpIHsgcmV0dXJuIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChvYmplY3QsIHByb3BlcnR5KTsgfTtcblxuIFx0Ly8gX193ZWJwYWNrX3B1YmxpY19wYXRoX19cbiBcdF9fd2VicGFja19yZXF1aXJlX18ucCA9IFwiXCI7XG5cblxuIFx0Ly8gTG9hZCBlbnRyeSBtb2R1bGUgYW5kIHJldHVybiBleHBvcnRzXG4gXHRyZXR1cm4gX193ZWJwYWNrX3JlcXVpcmVfXyhfX3dlYnBhY2tfcmVxdWlyZV9fLnMgPSBcIi4vc3JjL3N1cHJlbWUvaW5kZXgudHNcIik7XG4iLCJleHBvcnQgY2xhc3MgQ3Jhd2xlciB7XG5cbiAgICAvLyBwcml2YXRlIG11dGF0aW9uT2JzZXJ2ZXI6IE11dGF0aW9uT2JzZXJ2ZXI7XG5cbiAgICBjb25zdHJ1Y3RvciAoKSB7XG4gICAgICAgIC8vIHRoaXMubXV0YXRpb25PYnNlcnZlciA9IG5ldyBNdXRhdGlvbk9ic2VydmVyKHRoaXMub25NdXRhdGlvbi5iaW5kKHRoaXMpKTtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBDYWxsYmFjayBvbiBkb20gbXV0YXRpb25cbiAgICAgKiBAcGFyYW0gbXV0YXRpb25zIFxuICAgICAqL1xuICAgIC8vIG9uTXV0YXRpb24gKG11dGF0aW9uczogTXV0YXRpb25SZWNvcmQpIHtcbiAgICAvLyAgICAgY29uc29sZS5sb2cobXV0YXRpb25zKTtcbiAgICAvLyB9XG5cdFxuXHRmaW5kPFQgZXh0ZW5kcyBFbGVtZW50ID0gRWxlbWVudD4gKHNlbGVjdG9yOiBzdHJpbmcpOiBUIHwgbnVsbCB7XG5cdFx0cmV0dXJuIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3I8VD4oc2VsZWN0b3IpO1xuXHR9XG5cdFxuXHRmaW5kSW5wdXQgKHNlbGVjdG9yOiBzdHJpbmcpOiBIVE1MSW5wdXRFbGVtZW50IHwgbnVsbCB7XG5cdFx0cmV0dXJuIHRoaXMuZmluZDxIVE1MSW5wdXRFbGVtZW50PihzZWxlY3Rvcik7XG5cdH1cblx0XG5cdC8qKlxuXHQgKiBGaW5kIGEgbGluayB0byB0aGUgZ2l2ZW4gcGF0aCBmcm9tIHRoZSBjdXJyZW50IHBhZ2UuIFNob3VsZCBiZSBvdmVycmlkZW4gdG8gYmUgbW9yZSBzcGVjaWZpY1xuXHQgKiBAcGFyYW0gcGF0aCBcblx0ICovXG5cdGZpbmRMaW5rIChwYXRoOiBzdHJpbmcpOiBIVE1MRWxlbWVudCB7XG5cdFx0cmV0dXJuIHRoaXMuZmluZChwYXRoKTtcblx0fVxuXG5cdGZpbGxJbnB1dCAoZWxlbWVudDogSFRNTElucHV0RWxlbWVudCwgdGV4dDogc3RyaW5nKSB7XG5cdFx0ZWxlbWVudC52YWx1ZSA9IHRleHQ7XG5cdH1cblxuICAgIC8qKlxuICAgICAqIENsaWNrIG9uIGFuIGVsZW1lbnRcbiAgICAgKiBAcGFyYW0gZWxlbWVudCBcbiAgICAgKi9cbiAgICBhc3luYyBjbGljayAoZWxlbWVudDogSFRNTEVsZW1lbnQpOiBQcm9taXNlPHZvaWQ+IHtcbiAgICAgICAgLy8gdGhpcy5tdXRhdGlvbk9ic2VydmVyLm9ic2VydmUoZG9jdW1lbnQuYm9keSwge1xuICAgICAgICAvLyAgICAgYXR0cmlidXRlczogdHJ1ZSxcbiAgICAgICAgLy8gICAgIHN1YnRyZWU6IHRydWVcbiAgICAgICAgLy8gfSk7XG4gICAgICAgIGVsZW1lbnQuY2xpY2soKTtcblxuICAgICAgICAvLyB0aGlzLm11dGF0aW9uT2JzZXJ2ZXIuZGlzY29ubmVjdCgpO1xuICAgIH1cblxufSIsImltcG9ydCB7IElTZWxsZXIgfSBmcm9tICcuLi9JU2VsbGVyJztcbmltcG9ydCB7IENyYXdsZXIgfSBmcm9tICcuLi9DcmF3bGVyJztcbmltcG9ydCB7IElDaGVja291dEluZm9ybWF0aW9uIH0gZnJvbSAnLi9JQ2hlY2tvdXRJbmZvcm1hdGlvbic7XG5pbXBvcnQgeyBJQ2hlY2tvdXRGb3JtIH0gZnJvbSAnLi9JQ2hlY2tvdXRGb3JtJztcblxuLy8gSURFQSBwbHVnaW4gc3lzdGVtLCBhZGQgbmV3IGNsYXNzZXMuIERlZmluZSBJbnRlcmZhY2VzIChUUylcblxuZXhwb3J0IGNsYXNzIFN1cHJlbWUgZXh0ZW5kcyBDcmF3bGVyIGltcGxlbWVudHMgSVNlbGxlciB7XG5cblx0ZmluZExpbmsgKHBhdGg6IHN0cmluZyk6IEhUTUxFbGVtZW50IHtcblx0XHRyZXR1cm4gdGhpcy5maW5kKHBhdGgpXG4gICAgICAgIHx8IHRoaXMuZmluZChgaHR0cDovLyR7U3VwcmVtZS5ob3N0bmFtZX0ke3BhdGh9YClcbiAgICAgICAgfHwgdGhpcy5maW5kKGBodHRwczovLyR7U3VwcmVtZS5ob3N0bmFtZX0ke3BhdGh9YCk7XG5cdH1cblxuICAgIGdldFByb2R1Y3RzICgpIHtcbiAgICAgICAgZm9yIChjb25zdCBjYXRlZ29yeSBvZiBTdXByZW1lLmNhdGVnb3JpZXMpIHtcbiAgICAgICAgICAgIGNvbnN0IGxpbmsgPSB0aGlzLmZpbmRMaW5rKGAvc2hvcC9hbGwvJHtjYXRlZ29yeX1gKTtcbiAgICAgICAgICAgIGlmICghbGluaykgY29udGludWU7XG4gICAgICAgICAgICB0aGlzLmNsaWNrKGxpbmspO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiBbXTtcbiAgICB9XG5cblx0LyoqXG5cdCAqIEdldCBjaGVja291dCBpbmZvcm1hdGlvbiAoZGF0YSBuZWVkZWQgYnkgdGhlIGZvcm0pXG5cdCAqL1xuXHRnZXRDaGVja291dEluZm9ybWF0aW9uICgpOiBJQ2hlY2tvdXRJbmZvcm1hdGlvbiB7XG5cdFx0cmV0dXJuIHtcblx0XHRcdG5hbWU6ICdKb2huIERvZScsXG5cdFx0XHRlbWFpbDogJ2R1bW15QGVtYWlsLmxvbCcsXG5cdFx0XHRwaG9uZTogJzAwMDAwMDAwMDAnLFxuXHRcdFx0YWRkcmVzczogJzMzMCBhbGzDqWUgZGVzIFVyc3VsaW5lcycsXG5cdFx0XHRhcHB0OiAnMzAyJyxcblx0XHRcdHppcGNvZGU6ICdHNUwgMlo5Jyxcblx0XHRcdGNpdHk6ICdSaW1vdXNraScsXG5cdFx0XHRjb3VudHJ5OiAnQ0FOQURBJyxcblx0XHRcdHN0YXRlOiAnUUMnLFxuXHRcdFx0bnVtYmVyOiAnNTEwMDAwMDAwMDAwMDAxNicsXG5cdFx0XHRleHBNb250aDogJzEyJyxcblx0XHRcdGV4cFllYXI6ICcyMDIxJyxcblx0XHRcdGN2djogJzEyMycsXG5cdFx0fTtcblx0XHQvLyBNdXN0IHNldCBjb3VudHJ5IGZpcnN0LCB0aGVuIHN0YXRlXG5cdH1cblxuXHQvKipcblx0ICogR2V0IGNoZWNrb3V0IGZvcm0gZWxlbWVudHNcblx0ICovXG5cdGdldENoZWNrb3V0Rm9ybSAoKTogSUNoZWNrb3V0Rm9ybSB7XG5cdFx0bGV0IGVsZW1lbnRzOiBJQ2hlY2tvdXRGb3JtID0ge30gYXMgSUNoZWNrb3V0Rm9ybTtcblxuXHRcdGZvciAoY29uc3QgW2tleSwgc2VsZWN0b3JdIG9mIE9iamVjdC5lbnRyaWVzKFN1cHJlbWUuY2hlY2tvdXRTZWxlY3RvcnMpKSB7XG5cdFx0XHRlbGVtZW50c1trZXldID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcihzZWxlY3Rvcik7XG5cdFx0fVxuXHRcdHJldHVybiBlbGVtZW50cztcblx0fVxuXG5cdC8qKlxuXHQgKiBGaWxsIGNoZWNrb3V0IGluZm9ybWF0aW9uIGFuZCBwcm9jZWVkLlxuXHQgKiBOZWVkcyB0byBhbHJlYWR5IGJlIG9uIGNoZWNrb3V0IHBhZ2UuXG5cdCAqL1xuXHRjaGVja291dCAoKSB7XG5cdFx0bGV0IGRhdGE6IElDaGVja291dEluZm9ybWF0aW9uID0gdGhpcy5nZXRDaGVja291dEluZm9ybWF0aW9uKCk7XG5cdFx0bGV0IGVsZW1lbnRzOiBJQ2hlY2tvdXRGb3JtID0gdGhpcy5nZXRDaGVja291dEZvcm0oKTtcblx0XHRcblx0XHR0cnkge1xuXHRcdFx0Ly8gRmlsbCByZWd1bGFyIGlucHV0XG5cdFx0XHRmb3IgKGNvbnN0IGtleSBpbiBkYXRhKSB7XG5cdFx0XHRcdHRoaXMuZmlsbElucHV0KGVsZW1lbnRzW2tleV0sIGRhdGFba2V5XSk7XG5cdFx0XHR9XG5cdFx0XHQvLyBDaGVjayBUT1MgY2hlY2tib3hcblx0XHRcdGVsZW1lbnRzLnRvcy5jaGVja2VkID0gdHJ1ZTtcblx0XHRcdC8vIFN1Ym1pdCBmb3JtXG5cdFx0XHR0aGlzLmNsaWNrKGVsZW1lbnRzLnN1Ym1pdCk7XG5cdFx0XHQvLyBjb25zdCBlID0gdGhpcy5maW5kSW5wdXQoU3VwcmVtZS5jaGVja291dFNlbGVjdG9ycy5uYW1lKTtcblx0XHRcdC8vIHRoaXMuZmlsbElucHV0KGUsICdsb2wnKTtcblx0XHR9IGNhdGNoIChlcnIpIHtcblx0XHRcdGNvbnNvbGUubG9nKGVycik7XG5cdFx0fVxuXHR9XG5cbiAgICAvKipcbiAgICAgKiBXZWJzaXRlIGhvc3RuYW1lXG4gICAgICovXG4gICAgc3RhdGljIGdldCBob3N0bmFtZSAoKSB7XG4gICAgICAgIHJldHVybiAnd3d3LnN1cHJlbWVuZXd5b3JrLmNvbSc7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogQXZhaWxhYmxlIHByb2R1Y3QgY2F0ZWdvcmllc1xuICAgICAqL1xuICAgIHN0YXRpYyBnZXQgY2F0ZWdvcmllcyAoKTogc3RyaW5nW10ge1xuICAgICAgICByZXR1cm4gW1xuICAgICAgICAgICAgJ2phY2tldHMnLFxuICAgICAgICAgICAgJ3NoaXJ0cycsXG4gICAgICAgICAgICAndG9wcy9zd2VhdGVycycsXG4gICAgICAgICAgICAnc3dlYXRzaGlydHMnLFxuICAgICAgICAgICAgJ3BhbnRzJyxcbiAgICAgICAgICAgICdoYXRzJyxcbiAgICAgICAgICAgICdiYWdzJyxcbiAgICAgICAgICAgICdhY2Nlc3NvcmllcycsXG4gICAgICAgICAgICAnc2thdGUnXG4gICAgICAgIF07XG5cdH1cblx0XG5cdC8qKlxuXHQgKiBDU1Mgc2VsZWN0b3JzIG9uIHRoZSBjaGVja291dCBwYWdlXG5cdCAqIE5PVEU6IG9ubHkgb24gVVMgdmVyc2lvbiBvbmx5XG5cdCAqL1xuXHRzdGF0aWMgZ2V0IGNoZWNrb3V0U2VsZWN0b3JzICgpIHtcblx0XHRyZXR1cm4ge1xuXHRcdFx0bmFtZTogJyNvcmRlcl9iaWxsaW5nX25hbWUnLFxuXHRcdFx0ZW1haWw6ICcjb3JkZXJfZW1haWwnLFxuXHRcdFx0cGhvbmU6ICcjb3JkZXJfdGVsJyxcblx0XHRcdGFkZHJlc3M6ICcjYm8nLFxuXHRcdFx0YXBwdDogJyNvYmEzJyxcblx0XHRcdHppcGNvZGU6ICcjb3JkZXJfYmlsbGluZ196aXAnLFxuXHRcdFx0Y2l0eTogJyNvcmRlcl9iaWxsaW5nX2NpdHknLFxuXHRcdFx0Y291bnRyeTogJyNvcmRlcl9iaWxsaW5nX2NvdW50cnknLFxuXHRcdFx0c3RhdGU6ICcjb3JkZXJfYmlsbGluZ19zdGF0ZScsXG5cdFx0XHRudW1iZXI6ICcjbm5hZXJiJyxcblx0XHRcdGV4cE1vbnRoOiAnI2NyZWRpdF9jYXJkX21vbnRoJyxcblx0XHRcdGV4cFllYXI6ICcjY3JlZGl0X2NhcmRfeWVhcicsXG5cdFx0XHRjdnY6ICcjb3JjZXInLFxuXHRcdFx0dG9zOiAnI29yZGVyX3Rlcm1zJyxcblx0XHRcdHN1Ym1pdDogJyNwYXkgPiBpbnB1dCdcblx0XHR9O1xuXHR9XG5cbiAgICAvKipcbiAgICAgKiBOYXZpZ2F0aW9uIHVybHNcbiAgICAgKi9cbiAgICBzdGF0aWMgZ2V0IFVSTFMgKCkge1xuICAgICAgICByZXR1cm4ge1xuICAgICAgICAgICAgQkFTRTogJy8nLFxuICAgICAgICAgICAgU0hPUDoge1xuICAgICAgICAgICAgICAgIEJBU0U6ICcvc2hvcCcsXG4gICAgICAgICAgICAgICAgQUxMOiAnL3Nob3AvYWxsJyxcbiAgICAgICAgICAgICAgICBORVc6ICcvc2hvcC9uZXcnLFxuICAgICAgICAgICAgICAgIEpBQ0tFVFM6ICcvc2hvcC9qYWNrZXRzJyxcbiAgICAgICAgICAgICAgICBTSElSVFM6ICcvc2hvcC9zaGlydHMnLFxuICAgICAgICAgICAgICAgIFRPUFNfU1dFQVRFUlM6ICcvc2hvcC90b3BzX3N3ZWF0ZXJzJyxcbiAgICAgICAgICAgICAgICBTV0VBVFNISVJUUzogJy9zaG9wL3N3ZWF0c2hpcnRzJyxcbiAgICAgICAgICAgICAgICBQQU5UUzogJy9zaG9wL3BhbnRzJyxcbiAgICAgICAgICAgICAgICBIQVRTOiAnL3Nob3AvaGF0cycsXG4gICAgICAgICAgICAgICAgQkFHUzogJy9zaG9wL2JhZ3MnLFxuICAgICAgICAgICAgICAgIEFDQ0VTU09SSUVTOiAnL3Nob3AvYWNjZXNzb3JpZXMnLFxuICAgICAgICAgICAgICAgIFNLQVRFUzogJy9zaG9wL3NrYXRlJyxcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICBDQVJUOiAnL2NhcnQnLFxuICAgICAgICAgICAgQ0hFQ0tPVVQ6ICcvY2hlY2tvdXQnLFxuICAgICAgICB9O1xuICAgIH1cblxufVxuIiwiLyoqXG4gKiBUaGlzIHNjcmlwdCBpcyBpbnRlbmRlZCB0byBiZSBydW4gaW4gdGhlIGNvbnRleHQgb2YgdGhlIFN1cHJlbWUgb25saW5lIHNob3AuXG4gKi9cblxuaW1wb3J0IHsgU3VwcmVtZSB9IGZyb20gJy4vU3VwcmVtZSc7XG5cbmxldCBzdXByZW1lID0gbmV3IFN1cHJlbWUoKTtcblxuZnVuY3Rpb24gZ29Ub1Nob3AgKCkge1xuXHRpZiAoIXdpbmRvdy5sb2NhdGlvbi5ocmVmLmluY2x1ZGVzKFN1cHJlbWUuVVJMUy5TSE9QLkJBU0UpKSB7XG5cdFx0Y29uc3Qgc2hvcExpbmsgPSBzdXByZW1lLmZpbmRMaW5rKFN1cHJlbWUuVVJMUy5TSE9QLkJBU0UpO1xuXHRcdGlmIChzaG9wTGluaykge1xuXHRcdFx0c2hvcExpbmsuY2xpY2soKTtcblx0XHR9IGVsc2Uge1xuXHRcdFx0dGhyb3cgbmV3IEVycm9yKCdDYW5ub3QgZmluZCBhIGxpbmsgdG8gdGhlIHNob3AuIEFib3J0aW5nJyk7XG5cdFx0fVxuXHR9IGVsc2Uge1xuXHRcdGNvbnNvbGUud2FybignQWxyZWFkeSBvbiBzaG9wIHBhZ2UnKTtcblx0fVxufVxuXG50cnkge1xuXHQvLyBnb1RvU2hvcCgpO1xuXHRzdXByZW1lLmNoZWNrb3V0KCk7XG59IGNhdGNoIChlcnIpIHtcblx0Y29uc29sZS5lcnJvcihlcnIpO1xufVxuXG4vLyBzZXRJbnRlcnZhbChfID0+IHtcbi8vICAgICBjb25zb2xlLmxvZyh3aW5kb3cubG9jYXRpb24uaHJlZik7XG4vLyB9LCAxMDAwKVxuIl0sInNvdXJjZVJvb3QiOiIifQ==