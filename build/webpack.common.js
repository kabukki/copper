const path = require('path');

module.exports = {
    entry: {
        background: path.join(__dirname, '../src/background.ts'),
        popup: path.join(__dirname, '../src/popup.ts'),
		options: path.join(__dirname, '../src/options.ts'),
		// Content scripts - one per website
        supreme: path.join(__dirname, '../src/supreme/index.ts'),
    },
    output: {
        path: path.join(__dirname, '../extension/js'),
        filename: '[name].js'
    },
    module: {
        rules: [
			{ test: /\.ts$/, use: 'ts-loader', exclude: /node_modules/ }
        ]
    },
    resolve: {
        extensions: ['.ts', '.js']
    }
};
